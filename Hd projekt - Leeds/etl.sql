CREATE TABLE Temp
(
	[Reference Number] varchar(50),
	[Easting] int,
	[Northing] int,
    [Number of Vehicles] int,
    [Accident Date] varchar(50),
    [Time (24hr)] int,
    [1st Road Class] varchar(50),
    [Road Surface] varchar(50),
    [Lighting Conditions] varchar(50),
    [Weather Conditions] varchar(50),
    [Casualty Class] varchar(50),
    [Casualty Severity] varchar(50),
    [Sex of Casualty] varchar(10),
    [Age of Casualty] int,
    [Type of Vehicle] varchar(100)
 );
 
----- Poprawienie formatu dat wypadk�w w tabeli Temp
CREATE TABLE Temp2
(
    [Reference Number] varchar(50),
    [Easting] int,
    [Northing] int,
    [Number of Vehicles] int,
    [Accident Date] varchar(50),
    [Time (24hr)] int,
    [1st Road Class] varchar(50),
    [Road Surface] varchar(50),
    [Lighting Conditions] varchar(50),
    [Weather Conditions] varchar(50),
    [Casualty Class] varchar(50),
    [Casualty Severity] varchar(50),
    [Sex of Casualty] varchar(10),
    [Age of Casualty] int,
    [Type of Vehicle] varchar(100)
 );

 
INSERT INTO Temp2([Reference Number], [Easting], [Northing],[Number of Vehicles], [Accident Date] , [Time (24hr)], [1st Road Class], [Road Surface], [Lighting Conditions],  [Weather Conditions], [Casualty Class],  [Casualty Severity], [Sex of Casualty], [Age of Casualty],[Type of Vehicle])

SELECT [Reference Number], [Easting], [Northing],[Number of Vehicles], 
convert(date,[Accident Date], 105), 
[Time (24hr)], [1st Road Class], [Road Surface], [Lighting Conditions],  [Weather Conditions], [Casualty Class],  [Casualty Severity], [Sex of Casualty], [Age of Casualty],[Type of Vehicle]
FROM Temp;


DROP TABLE Temp;



CREATE TABLE Temp
(
   [Reference Number] varchar(50),
   [Easting] int,
   [Northing] int,
    [Number of Vehicles] int,
    [Accident Date] varchar(50),
    [Time (24hr)] int,
    [1st Road Class] varchar(50),
    [Road Surface] varchar(50),
    [Lighting Conditions] varchar(50),
    [Weather Conditions] varchar(50),
    [Casualty Class] varchar(50),
    [Casualty Severity] varchar(50),
    [Sex of Casualty] varchar(10),
    [Age of Casualty] int,
    [Type of Vehicle] varchar(100)
 );

INSERT INTO Temp
SELECT * FROM TEMP2;

DROP TABLE Temp2;
--------------------------------------------


------------------ Tworzenie tabel hurotwnianych: -----------------
CREATE TABLE DIM_Date
(
	[DateKey] BIGINT PRIMARY KEY,
	[Year] INT NOT NULL,
	[Semester] INT NOT NULL,
	[Quarter] INT NOT NULL,
	[NumberOfMonth] INT NOT NULL,
	[DayNumberOfMonth] INT NOT NULL
);

CREATE TABLE DIM_Time
(
  [TimeKey] BIGINT PRIMARY KEY,
  [Hour] INT NOT NULL,
  [Minutes] INT NOT NULL
);

CREATE TABLE DIM_Vehicle
(
	[VehicleKey] BIGINT IDENTITY PRIMARY KEY,
	[VehicleType] VARCHAR(100) NOT NULL
);

CREATE TABLE DIM_Location 
(
	[LocationKey] BIGINT IDENTITY PRIMARY KEY,
	[Easting] INT,
	[Northing] INT,
	[RoadClass] VARCHAR(50) NOT NULL	
);

CREATE TABLE DIM_Condition 
(
	[ConditionKey] BIGINT IDENTITY PRIMARY KEY,
	[Lighting] VARCHAR(50) NOT NULL,
	[Weather] VARCHAR(50) NOT NULL,
	[RoadSurface] VARCHAR(50) NOT NULL
);

CREATE TABLE DIM_Casualty
(
	[CasualtyKey] BIGINT IDENTITY PRIMARY KEY,
	[Class] VARCHAR(50) NOT NULL,
	[Severity] VARCHAR(50) NOT NULL,
	[Sex] VARCHAR(10) NOT NULL,
	[Age] INT NOT NULL
);


CREATE TABLE FACT_Accident 
(
	[AccidentKey] BIGINT PRIMARY KEY IDENTITY,
	[ReferenceNumber] VARCHAR(50) NOT NULL,
	NumberOfVehicles INT NOT NULL,

	[Date] BIGINT REFERENCES DIM_Date([DateKey]),
	[Time] BIGINT REFERENCES DIM_TIME([TimeKey]),
	Vehicle BIGINT REFERENCES DIM_Vehicle([VehicleKey]),
	Location BIGINT REFERENCES DIM_Location([LocationKey]),
	Casualty BIGINT REFERENCES DIM_Casualty([CasualtyKey]),
	Condition BIGINT REFERENCES DIM_Condition([ConditionKey])
);


------------------ import danych z tabli Temp do wymiar�w-----------------

INSERT INTO DIM_Date
SELECT DISTINCT YEAR([Accident Date])*10000+MONTH([Accident Date])*100+DAY([Accident Date]), YEAR([Accident Date]),
(CASE WHEN(MONTH([Accident Date]) <=6) THEN 1 ELSE 2 END),
(
	CASE WHEN MONTH([Accident Date]) BETWEEN 1 AND 3 THEN 1
	     WHEN MONTH([Accident Date]) BETWEEN 4 AND 6 THEN 2
		 WHEN MONTH([Accident Date]) BETWEEN 7 AND 9 THEN 3
		 ELSE 4
END),
MONTH([Accident Date]),
DAY([Accident Date])
FROM TEMP;
		 

INSERT INTO DIM_Time 
SELECT DISTINCT [Time (24hr)], [Time (24hr)]/100, [Time (24hr)]%100 FROM Temp;

INSERT INTO DIM_Vehicle (VehicleType)
SELECT DISTINCT [Type of Vehicle] FROM Temp;

INSERT INTO DIM_Location (Easting, Northing, RoadClass)
SELECT DISTINCT Easting, Northing, [1st Road Class] FROM Temp;


INSERT INTO DIM_Condition (Lighting, Weather, RoadSurface)
SELECT DISTINCT [Lighting Conditions], [Weather Conditions], [Road Surface] FROM Temp;

INSERT INTO DIM_Casualty (Class, Severity, Sex, Age)
SELECT DISTINCT [Casualty Class], [Casualty Severity], [Sex of Casualty], [Age of Casualty] FROM Temp;



------------------ import miar do tabeli fakt�w-----------------
INSERT INTO [FACT_Accident]
SELECT tmp.[Reference Number], tmp.[Number of Vehicles], 
	YEAR(tmp.[Accident Date])*10000+MONTH(tmp.[Accident Date])*100+DAY(tmp.[Accident Date]), tmp.[Time (24hr)],
	veh.VehicleKey, loc.LocationKey, cas.CasualtyKey, con.ConditionKey
FROM [Temp] tmp
JOIN DIM_Vehicle veh ON veh.VehicleType = tmp.[Type of Vehicle]
JOIN DIM_Location loc ON loc.Easting = tmp.Easting AND loc.Northing = tmp.Northing AND loc.RoadClass = tmp.[1st Road Class]
JOIN DIM_Casualty cas ON cas.Class = tmp.[Casualty Class] AND cas.Severity = tmp.[Casualty Severity] AND cas.Sex = tmp.[Sex of Casualty] AND cas.Age = tmp.[Age of Casualty]
JOIN DIM_Condition con ON con.Lighting = tmp.[Lighting Conditions] AND con.RoadSurface = tmp.[Road Surface] AND con.Weather = tmp.[Weather Conditions];



------------------ dodanie miary "Liczba ofiar wypadk�w'-----------------
ALTER TABLE FACT_Accident ADD [Number of Casualties] INT;
UPDATE F
SET F.[Number of Casualties] = T.numbers
FROM FACT_Accident F JOIN (SELECT [Reference Number] AS ref, COUNT([Reference Number]) AS numbers  FROM Temp GROUP BY [Reference Number]) T
ON T.ref = F.[ReferenceNumber];


--poza etl----------------
UPDATE DIM_Condition
SET [RoadSurface] = 'Frost / Ice'
WHERE [RoadSurface] = 'Frost/ Ice';


---------------------------------w Cube-----------------------------------
(CASE	  WHEN [NumberOfMonth] = 1 THEN 'January'
          WHEN [NumberOfMonth] = 2 THEN 'February'
          WHEN [NumberOfMonth] = 3 THEN 'March'
          WHEN [NumberOfMonth] = 4 THEN 'April'
          WHEN [NumberOfMonth] = 5 THEN 'May'
          WHEN [NumberOfMonth] = 6 THEN 'June'
          WHEN [NumberOfMonth] = 7 THEN 'July'
          WHEN [NumberOfMonth] = 8 THEN 'August'
          WHEN [NumberOfMonth] = 9 THEN 'September'
          WHEN [NumberOfMonth] = 10 THEN 'October'
          WHEN [NumberOfMonth] = 11 THEN 'November'
          WHEN [NumberOfMonth] = 12 THEN 'December'
END)


	SELECT [NumberOfMonth], [DayNumberOfMonth],
	(CASE
			 WHEN(NumberOfMonth = 4 OR NumberOfMonth = 5 OR (NumberOfMonth = 3 AND DayNumberOfMonth >=22)  OR (NumberOfMonth = 6 AND DayNumberOfMonth <=21)) THEN 'Spring'
			WHEN(NumberOfMonth = 7 OR NumberOfMonth = 8 OR (NumberOfMonth = 6 AND DayNumberOfMonth >=22)  OR (NumberOfMonth = 9 AND DayNumberOfMonth <=22)) THEN 'Summer'
			WHEN(NumberOfMonth = 10 OR NumberOfMonth = 11 OR (NumberOfMonth = 9 AND DayNumberOfMonth >=23)  OR (NumberOfMonth = 12 AND DayNumberOfMonth <=21)) THEN 'Autum'
		    WHEN(NumberOfMonth <= 2  OR (NumberOfMonth = 12 AND DayNumberOfMonth >=22)  OR (NumberOfMonth = 3 AND DayNumberOfMonth <=21)) THEN 'Winter' 
	END) AS "Seasons"
FROM Dim_Date;



SELECT Age, 
CASE 
   WHEN Age < 18 THEN 'Underage'
   WHEN Age BETWEEN 19 AND 30 THEN 'Youth'
   WHEN Age BETWEEN 31 AND 50 THEN  'Adults'	
   WHEN Age BETWEEN 51 AND 67 THEN  'Elders'
   ELSE 'Seniors'
END
FROM DIM_Casualty;


SELECT [Hour],
CASE 
   WHEN [Hour] >=22 OR [Hour] BETWEEN 0 AND 5 THEN 'Night'
   WHEN [Hour] BETWEEN 6  AND 11 THEN 'Morning'
   WHEN [Hour] BETWEEN 12 AND 17 THEN 'Midday'	
   WHEN [Hour] BETWEEN 18 AND 21 THEN 'Evening'
END
FROM DIM_Time;













